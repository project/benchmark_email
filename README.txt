README.txt for Benchmark Email module (benchmark_email)
Last update: 6/17/2010

Module written by Erik Peterson, sponsored by Curt Keller and Benchmark Email, Inc.



OVERVIEW
========

This module integrates the Benchmark Email system with Drupal. It handles user subscriptions and unsubscriptions
seamlessly between Drupal and Benchmark, allowing Drupal users to subscribe during registration or at any time
while at their Account Edit page.

Admins can choose which Contact Lists their users are allowed to subscribe to, as well as various title handline (for
example, you can configure Drupal to call Contact Lists "Newsletters" if you prefer).

By default, subscription updates are processed during Cron runs, but this can be changed to process immediately via
an XMLRPC (cURL) call to the Benchmark Email API. Make sure cURL is enabled for your server.



INSTALLATION
============

Simply download the module tarball to your modules directory (drush users, type drush dl benchmark_email) and enable.



CONFIGURATION
=============

The first thing you'll want to do is visit admin/settings/benchmark_email and configure some things. You'll need to
enter your Benchmark login and password to connect to the site and generate your first token.

Once this is done, you can choose to enable Contact Lists from Benchmark to be subscribed to by your users.
(NOTE: The Contact Lists, at this point MUST be created in Benchmark. There is no mechanism for creating Contact Lists
  in Drupal... yet.)

To do this, you will want to visit the "Contact Lists" section of the Benchmark Admin (admin/settings/benchmark_email/lists)
and decide which Contact Lists your users can be allowed to see and subscribe to. You can also change the Contact List settings
for backend features such as when to sync your lists settings (default is Cron) as well as Registration settings.)



TODO
====

- Currently the module does not support "Do Not Contact". This feature is coming soon.

- Also, there is no way to create a List or Email (for sending to lists). This is an advanced feature that may not get
  much traction as part of the Drupal module.

- Finally, this module will need to integrate with the Profile / Bio modules to allow for First Name / Last Name integration (and
eventually, other demographic information, such as Zip code, etc.) for detailed List data management in Benchmark.
