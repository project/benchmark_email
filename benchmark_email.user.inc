<?php
/**
 * Implementation of hook_user().
 */
function benchmark_email_user($op, &$edit, &$account, $category = NULL) {

  switch($op) {

    case 'form':
      if ($category == 'account') {
        $form['benchmark_email'] = array(
          '#type' => 'fieldset',
          '#collapsed' => false,
          '#collapsible' => TRUE,
          '#title' => t(variable_get('benchmark_email_label', 'Newsletters')),
          '#description' => t(variable_get('benchmark_email_register_text', 'Sign up for our newsletters!')),
        );
        $form['benchmark_email']['lists'] = benchmark_email_get_lists_form(FALSE, $account);
        return $form;
      }
      break;

    // User is updating their info.
    case 'update':
      $benchmark = new benchmark_email;
      $token = $benchmark->get_token();

      foreach ($edit as $key => $value) {
        // If this user element is a Benchmark Email list, check our status.
        if (strpos($key, 'benchmark_id') > 0) {
          $listID = substr($key, 18);

          // Check to see if the user has updated their email address first.
          if ($account->mail != $edit['mail']) {
            $benchmark->user_update_details($listID, $account, $account->mail, $edit['mail']);
          }

          // Only send an update to Benchmark if the user is changing their subscription status.
          //   or if they are signing up for the first time.
          if (!$account->key || $account->key && $value <> $account->$key) {
            if ($value == 1) {

              // Build an array of user details. This is needed for eventual address / profile support.
              $subscriber = $edit['mail'];

              // If we sync on cron, add the user to the queue.
              if (variable_get('benchmark_email_settings_sync', 'cron') == 'cron') {
                $sub = $benchmark->user_update_db($account->uid, $listID, $edit['mail'], BENCHMARK_EMAIL_STATUS_PENDING, FALSE);
              }
              // Otherwise, subscribe the user immediately.
              else {
                $sub = $benchmark->user_subscribe($subscriber, $account->uid, $listID);

                if ($sub) {
                  drupal_set_message(t('Successfully subscribed to list #%id', array('%id' => $listID)), 'notice');
                  $benchmark->user_update_db($account->uid, $listID, $edit['mail'], BENCHMARK_EMAIL_STATUS_ACTIVE); // Update the db immediately
                }
              }
            }
            // Value of this list element was 0, which means the user is unsubscribing. Was it something we said?
            else {
              $unsubscriber = $account->mail;
              $unsub = $benchmark->user_unsubscribe($unsubscriber, $account->uid, $listID);

              // NOTE: For simplicity, Unsubscriptions are handled immediately (not during Cron).
              if ($unsub) {
                drupal_set_message(t('Successfully unsubscribed from list #%id', array('%id' => $listID)), 'notice');
              }
            }
          }
        }
      }
      break;

    // User is registering.
    case 'register':
      $form = array();

      // Show the signup checkboxes on the registration form by default.
      if (variable_get('benchmark_email_settings_registration', 'none') == 'optin') {
        $form['benchmark_signup'] = array(
          '#type' => 'fieldset',
          '#collapsible' => FALSE,
          '#title' => t(variable_get('benchmark_email_settings_label', 'Newsletters')),
          '#description' => t(variable_get('benchmark_email_settings_register_text', 'Sign up for our newsletters!')),
        );
        $form['benchmark_signup']['benchmark_lists'] = benchmark_email_get_lists_form();
      }
      else {
        if (variable_get('benchmark_email_settings_registration', 'none') == 'silent') {
          $benchmark = new benchmark_email();

          $form['benchmark_signup']['benchmark_lists'] = array(
            '#type' => 'hidden',
            '#value' => implode(',', $benchmark->get_enabled_lists()),
          );
        }
        return $form;
      }
      break;

    case 'submit':
      //dsm('submit');
      //drupal_set_message(print_r($edit, true));
      //drupal_set_message(print_r($account, true));
      break;

    case 'insert':
      if ($edit['mail']) {
        $query = "SELECT be_id FROM {benchmark_email} WHERE email = '%s'";
        if ($result = db_fetch_object(db_query($query, $edit['mail']))) {
          db_query("UPDATE {benchmark_email} SET uid = %d WHERE be_id = %d", $edit['uid'], $result->be_id);
        }
      }
      break;

    case 'view':
      $benchmark = new benchmark_email();
      $lists = $benchmark->user_get_subscribed_lists();

      foreach($lists as $id => $name) {
        $names[] = $name;
      }

      // Show no lists if there are none active
      if ($lists) {
        $account->content['benchmark_email'] = array(
          '#type' => 'user_profile_category',
          '#title' => t(variable_get('benchmark_email_settings_label', 'Newsletters')),
          '#value' => t('test'),
        );
        $account->content['benchmark_email']['subscriptions'] = array(
          '#type' => 'user_profile_item',
          '#title' => t('Current subscriptions'),
          '#value' => implode(', ', $names).'<br/>'.t('You can !edit.',
            array('!edit' => l('edit your subscriptions in your Account Edit page', 'user/'. $account->uid .'/edit'))),
        );
      }
      break;

    // User is being logged in. Check to see if they subscribed to anything.
    case 'login':
      if (isset($account->data)) {
        $data = unserialize($account->data);
        $processed = FALSE;
        $benchmark = new benchmark_email();

        // Process hidden (automatic) subscriptions.
        if (isset($data['benchmark_lists'])) {
          foreach(explode(',', $data['benchmark_lists']) as $listID) {
            $subbed = $benchmark->user_subscribe($account->mail, $account->uid, $listID, 'register');
          }
          $data['benchmark_lists'] = NULL;
          $processed = TRUE;
        }

        // If subscription data is processed, we update the user data to remove it.
        if ($processed) {
          user_save($account, $data);
        }
      }

      break;

    // User is being deleted for misbehaving.
    case 'delete':
      db_query("UPDATE {benchmark_email} SET status = 0, sent = 0, updated = %d WHERE uid = %d", time(), $account->uid);
      //$benchmark = new benchmark_email();
      $benchmark->user_unsubscribe($account->mail, $account->uid);
      drupal_set_message(t('User %d queued to be unsubscribed from Benchmark Email.', array('%d' => $account->uid)));
  }

}
