<?php
/**
 * @file
 *  Class to handle all Benchmark Email API methods.
 */

class benchmark_email {
  var $login;
  var $pass;
  var $token;

  /**
   * Retrieve a token either from the session or via a new XMLRPC API call.
   * @return $token
      The token for the site.
   */
  function get_token() {
    $time = time();

    // If a token exists, but has expired, set it again.
    if (variable_get('benchmark_email_token', '') != '' && variable_get('benchmark_email_token_expires', $time) <= $time) {
      //dsm('token expired! need to set a new token');
      return $this->set_token();
    }
    // ... unless can return one from the database...
    else if (variable_get('benchmark_email_token', '') != '' && variable_get('benchmark_email_token_expires', $time) > $time) {
      $this->token = variable_get('benchmark_email_token', '');
      if ($this->token != '') {
        //dsm ('returning the valid token: '. $this->token);
        return $this->token;
      }
    }
    // .. No? That means a token doesn't exist, so we set one.
    else {
      //dsm('setting a new token');
      return $this->set_token();
    }
  }

  /**
   * Set a token.
   */
  public function set_token() {
    $login = check_plain(variable_get('benchmark_email_login', ''));
    $pass = check_plain(variable_get('benchmark_email_password', ''));

    if ($login == '' || $pass == '') {
      drupal_set_message('You need to enter your Benchmark API credentials!', 'error');
    }

    // Get a token from the API 'login' method.
    $this->token = xmlrpc(BENCHMARK_EMAIL_API, 'login', $login, $pass);

    // Add the token for this user via Benchmark API.
    xmlrpc(BENCHMARK_EMAIL_API, 'tokenAdd', $login, $pass, $this->token);

    // Make sure we get something back from API.
    if ($this->token == '' || !$this->token) {
      drupal_set_message('Could not initialize Benchmark token.');
    }
    else {
      // Store the token locally with an expiration of 12 hours, just to be safe.
      variable_set('benchmark_email_token', $this->token);
      variable_set('benchmark_email_token_expires', time() + 43200);
      return $this->token;
    }

  }

  /**
   * Retrieve all Contact lists from the Benchmark account.
   * @return array
   *  Returns an array of all lists that can be looped through.
   */
  public function get_lists() {

    $token = $this->get_token();

    $this->lists = xmlrpc(BENCHMARK_EMAIL_API, 'listGet', $token, "", 1, 100, "", "");
    if (!is_array($this->lists)) {
      drupal_set_message("Could not retrieve any lists. Check that your token hasn't expired.", 'error');
      return FALSE;
    }
    $this->list_mul = xmlrpc(BENCHMARK_EMAIL_API, 'listGet', $token, "", 1, 1, "date", "desc");
    //dsm($this->lists);
    return $this->lists;
  }

  /**
   * Return an array of just the lists that are enabled for the site.
   */
  public function get_enabled_lists() {
    $lists = $this->get_lists();
    $ret = array();

    foreach ($lists as $key => $list) {
      if (variable_get('benchmark_email_list_enabled_'. $list['id'], FALSE) == TRUE) {
        $ret[$list['id']]['id'] = $list['id'];
        $ret[$list['id']]['name'] = $list['listname'];
      }
    }

    if ($ret) {

      return $ret;
    }
    else {
      return FALSE;
    }
  }


  /** User functions **/

  public function user_get_subscribed_lists() {
    $lists = $this->get_enabled_lists();

    if (is_array($lists)) {
      $userlists = array();
      foreach($lists as $list) {
        $userlists[$list['id']] = $list['name'];
      }

      return $userlists;
    }
  }

  /**
   * Subscribe a user.
   * @param $user
   *  An object populated with user data, including id and mail.
   * @return BOOL
   *  Whether the operation was successful or not.
   */
  public function user_subscribe($email, $uid, $listID, $method = '') {
    $token = $this->get_token();

    $this->user_update_db($uid, $listID, $email, BENCHMARK_EMAIL_STATUS_ACTIVE, TRUE, $method);
    // TODO: Add Profile / Addresses support
    $success = xmlrpc(BENCHMARK_EMAIL_API, 'listAddContacts', $token,  $listID, array(array('email' => $email)));

    // Result will be # of contacts added to list.
    if ($success > 0) {
       return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Subscribe multiple users.
   *
   * @param $listID int
   *  The Benchmark identification number for a Contact List.
   * @param $emails array
   *  An array containing associative arrays of emails.
   * @param $method string
   *  (Optional) Where these subscribers came from
   */
  public function user_subscribe_multi($listID, $emails, $method = '') {

    $token = $this->get_token();
    $success = xmlrpc(BENCHMARK_EMAIL_API, 'listAddContacts', $token,  $listID, $emails);

    // Result will be # of contacts added to list.
    if ($success > 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Unsubscribe a user
   */
  public function user_unsubscribe($email, $uid, $listID = array(), $dnc = FALSE) {
    $token = $this->get_token();

    // TODO: Add Profile / Addresses support
    $success = xmlrpc(BENCHMARK_EMAIL_API, 'listDeleteEmailContact', $token,  $listID, $email);
    $this->user_update_db($uid, $listID, $email, BENCHMARK_EMAIL_STATUS_UNSUB);

    // Add the user to the Master Unsubscribe list, if directed.
    // NOTE: MUL id numbers will change.

    // Result is a BOOLEAN for some reason.
    if ($success) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Update a user in Benchmark
   */
  public function user_update_details($listID, $user, $old_email, $new_email) {

    $token = $this->get_token();

    if (!$listID || $listID == '' || intval($listID == 0)) {
      drupal_set_message('An invalid list ID was received. Please contact a site administrator.', 'error');
      return FALSE;
    }

    //dsm('looking to update for '.$listID);
    // Get the ID of the current user in Benchmark.
    $details = xmlrpc(BENCHMARK_EMAIL_API, 'listGetContacts', $token, intval($listID), check_plain($old_email));

    if (!$details) {
      drupal_set_message(t('Could not fetch email %old in listID %id', array('%old' => $old_email, '%id' => $listID)), 'error');
    }
    else {
      $updated = array();
      $updated['email'] = $new_email;
      //dsm('updating details to '.$new_email);

      $changed = xmlrpc(BENCHMARK_EMAIL_API, 'listUpdateContactDetails', $token,  intval($listID), intval($details[0]['id']), $updated);

      // Method returns an array, for some reason.
      if (is_array($changed)) {
        drupal_set_message(t('User details for list %id were changed.', array()));
        return TRUE;
      }
    }

    //$subscribe = $this->user_subscribe($user, $listID);
  }

  /**
   * Helper function to update the status in the database.
   */
  public function user_update_db($uid, $listID, $email, $new_status = BENCHMARK_EMAIL_STATUS_ACTIVE, $sent = TRUE, $method = '') {
    $is_update = db_result(db_query("SELECT `be_id` FROM {benchmark_email} WHERE `uid` = %d AND listID = %d",
      intval($uid), intval($listID)));

    $updates = new stdClass();

    if ($is_update) {
      $updates->be_id = $is_update;
    }

    $updates->uid = $uid;
    $updates->listID = $listID;
    $updates->email = $email;
    $updates->status = $new_status;
    $updates->sent = $sent;
    $updates->updated = time();
    $updates->method = $method;

    if ($is_update) {
      // If we're updating the row, do so here.
      drupal_write_record('benchmark_email', $updates, 'be_id');
    }
    else {
      drupal_write_record('benchmark_email', $updates);
    }

  }
}
