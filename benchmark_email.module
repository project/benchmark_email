<?php

DEFINE('BENCHMARK_EMAIL_API', 'http://api.benchmarkemail.com/1.0/');
DEFINE('BENCHMARK_EMAIL_STATUS_ACTIVE', 'active');
DEFINE('BENCHMARK_EMAIL_STATUS_PENDING', 'pending');
DEFINE('BENCHMARK_EMAIL_STATUS_UNSUB', 'unsub');

require_once ( dirname(__FILE__) . '/benchmark_email.api.inc' );
require_once ( dirname(__FILE__) . '/benchmark_email.user.inc' );

/**
* @file
 *  Integrates Drupal with the Benchmark Email email campaign management system.
 *  Note: You'll need an account from benchmarkemail.com.
*/
function benchmark_email_menu() {
  $items = array();

  $items['user/%user/benchmark_email'] = array(
    'title' => t('Benchmark Email Administration'),
    'type' => MENU_NORMAL_ITEM,
    //'access arguments' => array('access own benchmark email settings'),
    'page callback' => 'benchmark_email_admin',
    'file' => 'benchmark_email.admin.inc',
    'description' => 'Manage your Benchmark Email credentials and Contact Lists.',
  );

  $items['admin/settings/benchmark_email'] = array(
    'title' => t('Benchmark Email Administration'),
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer benchmark email'),
    'page callback' => 'benchmark_email_admin',
    'file' => 'benchmark_email.admin.inc',
    'description' => 'Manage your Benchmark Email credentials and Contact Lists.',
  );
  $items['admin/settings/benchmark_email/settings'] = array(
    'title' => t('Settings'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'access arguments' => array('administer benchmark email'),
    'page callback' => 'benchmark_email_admin',
    'file' => 'benchmark_email.admin.inc',
    'description' => 'Edit your Benchmark login and password.',
    'weight' => -10,
  );
  $items['admin/settings/benchmark_email/lists'] = array(
    'title' => t('Contact Lists'),
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('administer benchmark email'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('benchmark_email_lists_admin'),
    'file' => 'benchmark_email.admin.inc',
    'description' => 'Users can signup for Contact Lists. Manage them here.',
  );
  /*
  $items['admin/build/benchmark_email/emails'] = array(
    'title' => t('Campaigns'),
    'access arguments' => array('administer benchmark email'),
    'page callback' => 'benchmark_email_emails_admin',
    'type' => MENU_LOCAL_TASK,
  );*/
  return $items;
}



/**
 * Implememtation of hook_help().
 */
function benchmark_email_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/benchmark_email':
      return t('An API Token is provided by Benchmark Email automatically for use in Drupal.
        You only need to supply the login and password information for your Benchmark account.
        <strong>Note</strong> that Benchmark API Tokens are <strong>renewed automatically</strong>
        by this module.');

    case 'admin/settings/benchmark_email/lists':
      // Here is some help text for a custom page. This information will be
      //   introduced in the page when viewing example/foo URL.
      return t('Use this page to toggle individual Contact Lists on or off.
        Users will only be able to subscribe / unsubscribe from Contact Lists that are enabled.');
  }
}


/**
 * Build a form, for reuse in other areas of the site.
 *
 * @param boolean $admin
 *  Whether or not we are building for an admin area.
 * @return array $form
 *  Returns an array of checkboxes for injection into a form being built.
 *
 */
function benchmark_email_get_lists_form($admin = FALSE, $user = NULL) {
  $benchmark = new benchmark_email();
  $lists = $benchmark->get_lists();

  if (is_object($user)) {
    $data = unserialize($user->data);
  }

  foreach($lists as $list) {
    $user_status = db_result(db_query("SELECT status FROM {benchmark_email} WHERE uid = %d AND listID = %d", $user->uid, $list['id']));

    // Decide whether we're an admin or just a user looking to sign up.
    if ($admin == TRUE || ($admin == FALSE && variable_get('benchmark_email_list_enabled_'. $list['id'], FALSE) == TRUE)) {

      $form['list_benchmark_id_'. $list['id']] = array(
        '#type' => 'checkbox',
        '#title' => $list['listname'],
      );

      // If this is an Admin form, show whether the list is enabled for Users.
      if ($admin == TRUE) {
        $form['list_benchmark_id_'. $list['id']]['#description'] = t('Created: %date', array('%date' => $list['createdDate']));
        $form['list_benchmark_id_'. $list['id']]['#default_value'] = check_plain(variable_get('benchmark_email_list_enabled_'. $list['id'], FALSE));
      }
      // Otherwise show if a user is subscribed to this particular list
      else {
        // TODO: If there are no Benchmark IDs here, we should populate them via an API call.
        $form['list_benchmark_id_'. $list['id']]['#default_value'] = (!$user_status || $user_status == 'unsub') ? 0 : 1;
      }

      // Don't let the Admin select the MUL. There's no need.... unless we are getting the wrong ID.
      if ($list['id'] == $benchmark->list_mul[0]['id'] && $admin == TRUE) {
        $form['list_benchmark_id_'. $list['id']]['#disabled'] = TRUE;
        $form['list_benchmark_id_'. $list['id']]['#value'] = FALSE;
        $form['list_benchmark_id_'. $list['id']]['#description'] = t('The MUL is created by Benchmark Email and cannot be selected.');
      }
    }
  }

  // Hide the MUL from non-Admin forms.
  if ($admin == FALSE) {
    unset($form['list_benchmark_id_'. $benchmark->list_mul[0]['id']]);
  }

  return $form;
}


/**
 * Implementation of hook_perm().
 */
function benchmark_email_perm() {
   return array('subscribe to benchmark emails', 'administer benchmark email');
}

/**
 * Implementation of hook_access().
 */
function benchmark_email_access() {
  return user_access('subscribe to benchmark emails') || user_access('administer benchmark email');
}

/**
 * Access callback for user newsletter editing.
 */
function benchmark_email_edit_access($account) {
  global $user;
  // No anonymous..
  if (!$account || $account->uid == 0) {
    return FALSE;
  }

  // Allow users to edit their own subscription.
  if ($user->uid == $account->uid) {
    if (user_access('subscribe to benchmark emails', $account)) {
      return TRUE;
    }
  }

  // Allow admin to edit a user's subscription.
  if (user_access('administer users')) {
    return TRUE;
  }

  return FALSE;
}




/**
 * Implementation of hook_cron().
 *
 * Find updates since last cron run and push them to the server. If we're configured to do so.
 */
function benchmark_email_cron() {
  //if (variable_get('benchmark_email_settings_sync', 'onsubmit') == 'cron') {
    $crontime = variable_get('cron_last', time());
    $benchmark = new benchmark_email();

    // Loop through each enabled list, gathering the subscribers' info for queueing..
    $lists = $benchmark->get_enabled_lists();
    $emails = array();

    foreach($lists as $listID) {
      $emails[$listID] = array();

      // Get the Pending subscribers.
      $subs = db_query("SELECT * FROM {benchmark_email} WHERE `status` = %d AND `listID` = %d ORDER BY be_id LIMIT %d",
        BENCHMARK_EMAIL_STATUS_PENDING, $listID, variable_get('benchmark_email_batch_limit', 50));

      while ($sub = db_fetch_object($subs)) {
        // Build an array to be sent to the API call.
        $emails[$listID][]['email'] = $sub->email;
      }

      $send = $benchmark->user_subscribe_multi($listID, $emails[$listID]);
      if ($send) {
        // Set the account statuses to active.
        db_query("UPDATE {benchmark_email} SET status = 'active' WHERE status = %d AND `listID` = %d ORDER BY be_id LIMIT %d",
          BENCHMARK_EMAIL_STATUS_ACTIVE, $listID, variable_get('benchmark_email_batch_limit', 50));
      }
    }
  //}

}
