<?php
/**
 * Administer the settings area
 */
function benchmark_email_admin() {
  $output = '';

  $output .= drupal_get_form('benchmark_email_credentials_form');

  return $output;
}


/**
 * Build the Benchmark Email Settings form.
 *
 * This form contains the login credentials for the user's Benchmark account.
 *
 * TODO: Allow manual entering of a Benchmark token?
 */
function benchmark_email_credentials_form(&$form_state) {
  $benchmark = new benchmark_email();

  $be_login = check_plain(variable_get('benchmark_email_login', ''));
  $be_pass = check_plain(variable_get('benchmark_email_password', ''));

  $form = array();
  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['credentials']['benchmark_login'] = array(
    '#title' => t('Benchmark Username'),
    '#type' => 'textfield',
    '#default_value' => $be_login != '' ? $be_login : '',
    '#length' => 20,
    '#description' => t('Enter the login for your Benchmark Email account (usually your email address).'),
  );
  $form['credentials']['benchmark_password'] = array(
    '#title' => t('Benchmark Password'),
    '#type' => 'password',
    '#default_value' => $be_pass != '' ? $be_pass : '',
    '#length' => 20,
    '#description' => t('Enter the password for your Benchmark Email account.'),
  );
  $form['credentials']['token'] = array(
    '#title' => t('API Token'),
    '#value' => variable_get('benchmark_email_token', ''),
    '#default_value' => variable_get('benchmark_email_token', ''),
    '#disabled' => TRUE,
    '#description' => t('This is set via an API call to Benchmark, and cannot be edited. Will renew on: '.format_date(variable_get('benchmark_email_token_expires', time()), 'medium')),
    '#default_value' => '',
    '#type' => 'textfield',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Validate the form input
 */
function benchmark_email_credentials_form_validate($form, $form_state) {
  // TODO: check_plain all fields and try to login with the user's credentials.
  // The validation should fail if the credentials are incorrect, and only go forward on success.

}

/**
 * Handle form submission
 */
function benchmark_email_credentials_form_submit($form, &$form_state) {

  if ($form_state['values']['benchmark_login'] != '' && $form_state['values']['benchmark_password'] != '') {
    $be_login = check_plain($form_state['values']['benchmark_login']);
    $be_pass = check_plain($form_state['values']['benchmark_password']);

    $token = xmlrpc(BENCHMARK_EMAIL_API, 'login', $be_login, $be_pass);

    if ($token) {
      drupal_set_message('Your credentials were successfully saved! Your Benchmark Email account
        is now connected.');
      variable_set('benchmark_email_login', $be_login);
      //variable_set('benchmark_email_password', $be_pass);
      //variable_set('benchmark_email_token', check_plain($token));
      $_SESSION['benchmark_email_token'] = $token;
      xmlrpc(BENCHMARK_EMAIL_API, 'tokenAdd', $be_login, $be_pass, $token);
    }
    else {
     drupal_set_message('There was an error in the XMLRPC request.', 'error');
    }
  }

}

function benchmark_email_emails_admin() {
  $output = '';
  $benchmark = new benchmark_email();
  $token = $benchmark->get_token();

  $be_login = variable_get('benchmark_email_login', '');
  $be_pass = variable_get('benchmark_email_password', '');

  $campaigns = xmlrpc(BENCHMARK_EMAIL_API, 'emailGet', $token, "", "", 1, 100, "", "");

  // Loop through each campaign, and create a new checkbox to allow users to subscribe, and whether it's opt-in or opt-out.
  if ($campaigns) {
    foreach($campaigns as $campaign) {
      $form['campaigns'][$campaign['id']] = array(
        '#type' => 'markup',
        '#value' => $campaign['subject'],
      );
    }
  }
  else {
    $output = t('There are no campaigns yet!');
  }

  return $output;
}


/**
 * Administration area for Contact Lists (mailing lists that contain user emails)
 */
function benchmark_email_lists_admin($form) {

  $form = array();

  $form['benchmark_email_settings'] = array(
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#title' => t('Contact List settings')
  );

  $form['benchmark_email_settings']['registration'] = array(
    '#type' => 'select',
    '#title' => t('Show Contact List form on user registration page'),
    '#description' => t('Decide if users can sign up for your Benchmark lists
      during registration on your site. The "silent" option registers users automatically,
      without their consent.'),
    '#options' => array(
      'none' => 'Do not show the form, and do not register users automatically (default)',
      'silent' => 'Sign the user up for all Contact Lists during registration, silently',
      'optin' => 'Show the Contact List form on registration, allowing user to choose',
      ),
    '#default_value' => variable_get('benchmark_email_settings_registration', 'none'),
  );
  $form['benchmark_email_settings']['sync'] = array(
    '#type' => 'select',
    '#title' => t('Contact List sync'),
    '#description' => t('Choose whether users should be synced when they submit their Account
      forms or if it should be done at cron time.
      For larger sites, the Cron option is recommended.'),
    '#options' => array('cron' => 'During cron runs', 'onsubmit' => 'When a user submits'),
    '#default_value' => variable_get('benchmark_email_settings_sync', 'cron'),
  );
  $form['benchmark_email_settings']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label for Contact Lists'),
    '#default_value' => t(variable_get('benchmark_email_settings_label', 'Newsletters')),
    '#length' => 25,
    '#description' => t('How should we refer to Contact Lists? This is helpful for users.'),
  );
  $form['benchmark_email_settings']['signup_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Helpful signup text'),
    '#default_value' => t(variable_get('benchmark_email_settings_register_text', 'Sign up for our newsletters!')),
    '#length' => 50,
    '#description' => t('This text will describe to the user the various lists available to them.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#weight' => 10,
  );

  $form['benchmark_email'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Lists'),
    '#description' => t("If you have created Contact Lists in Benchmark, enable them here. (Users
      can only subscribe to a Contact List once it's enabled.)"),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  $form['benchmark_email']['lists'] = benchmark_email_get_lists_form($admin = TRUE);

  return $form;
}

/**
 * Handle the contact list form submission.
 */
function benchmark_email_lists_admin_submit($form, &$form_state) {

  foreach($form_state['values'] as $key => $value) {
    // We only care about benchmark checkboxes, not other form elements.
    if (strpos($key, 'benchmark_id') > 0) {
      $id = substr($key, 18);
      variable_set('benchmark_email_list_enabled_'. $id, check_plain($value));
    }
  }
  variable_set('benchmark_email_settings_registration', $form_state['values']['registration']);
  variable_set('benchmark_email_settings_sync', $form_state['values']['sync']);
  variable_set('benchmark_email_settings_label', $form_state['values']['label']);
  variable_set('benchmark_email_settings_register_text', $form_state['values']['signup_text']);
  drupal_set_message(t('Contact List changes were saved.'));
}
